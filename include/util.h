#ifndef _UTIL_H
#define _UTIL_H

#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc/imgproc.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "dirent.h"

double getOverlapRate(cv::Rect W1,cv::Rect W2);
int round (double x);
void loadGTlog(std::string labelDir,std::vector<std::vector<double> > &bboxGT, double Scale);
std::vector<cv::Rect> & catRect(std::vector<cv::Rect> &v1, std::vector<cv::Rect> &v2);
std::vector<double> & catVect(std::vector<double> & v1, std::vector<double> &v2);
bool fexists(const std::string& name);

#endif