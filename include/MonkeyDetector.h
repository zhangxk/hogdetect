#ifndef MONKEYDETECTOR_H
#define MONKEYDETECTOR_H

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>
#include <vector>
#include <string>
#include <fstream>
#include "svm.h"
#include "getFileNameList.h"
#include "util.h"

class MonkeyDetector:public cv::HOGDescriptor
{
public:
	MonkeyDetector();	
	~MonkeyDetector();
	void MonkeyDetector::train(std::string posInputDir, std::string negInputDir, std::string modelName,
		std::string inputType=".png", std::string logName="hog.txt", std::string svmModelName="svmModel");
	void detect(cv::Mat img, std::vector<cv::Rect> &bbox, std::vector<double> &hitWeights);
	void load(std::string modelName);
	void save(std::string modelName);
	void loadParameters(std::string modelName);
	void saveParameters(std::string modelName);
	void loadSVMModel(std::string modelName);	
	void colorFilter(cv::Mat &img, std::vector<cv::Rect> &bbox);
	void colorFilter(cv::Mat &img, std::vector<cv::Rect> &bbox, std::vector<double> &hitWeights);

	std::vector<double> scaleX;
	std::vector<double> scaleY;
	int winW_orig;
	int winH_orig;
	int winStepX;
	int winStepY;
	int nLevels;			// number of detecting levels in hog multi-scale detector
	double svm_thres;		// svm decision threshold
	double scaleStep;		// scale step parameter in hog multi-scale detector 
	double merge_thres;		// merging threshold in hog multi-scale detector
	bool mergeEnable;
	std::vector<float> w;	// svm classifier vector (for linear case only)
	double c;				// svm training regularization parameter
};

#endif