#include "getFileNameList.h"

std::vector<std::string> getFileNameList(std::string inputDir, const char * fileType)
{
	std::vector<std::string> fileNames;
	std::string sFileName;
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir (inputDir.c_str())) != NULL)
	{
		// print all the files and directories within directory and the file type
		while ((ent = readdir (dir)) != NULL)
		{			
			if (!strcmp(ent->d_name,".") || !strcmp(ent->d_name,"..")) continue;
			if (!strstr(ent->d_name,fileType)) continue;
			sFileName = inputDir + "\\";
			sFileName += ent->d_name;
			fileNames.push_back(sFileName.c_str());
			printf ("%s\n", sFileName.c_str());
		}
		closedir (dir);
		return fileNames;
	}
	else
	{
		/* could not open directory */
		perror ("");
		//return EXIT_FAILURE;
		throw;
	}
}