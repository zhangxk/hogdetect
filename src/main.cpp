
#include <vector>
#include "util.h"
#include "MonkeyDetector.h"
#ifdef _CHAR16T
#define CHAR16_T
#endif
#include "mex.h"
#include "mexopencv.hpp"
#include "MxArray.hpp"

void mexFunction(int nlhs, mxArray *plhs[],
	int nrhs, const mxArray *prhs[])
{
	if (nrhs < 1)
	{
        mexPrintf("To detect:\n");
        mexPrintf("[bbox,weights] = hogDetect('detect',model,I)\n");
        mexPrintf("To train:\n");
        mexPrintf("hogDetect('train', posInputDir, negInputDir, model)\n");
        mexErrMsgTxt("There is no input to the function.");
	}
	if (!mxIsChar(prhs[0]))
	{
		mexErrMsgTxt("The first parameter must be a string.");
	}
	std::string par1 = MxArray(prhs[0]).toString();
	if (par1 == "train")
	{
		if (nrhs != 4)
		{
			mexErrMsgTxt("Four input arguments required.");
		}
		if (!mxIsChar(prhs[1]) && !mxIsChar(prhs[2]) && !mxIsChar(prhs[3]))
		{
			mexErrMsgTxt("At least one of the parameters is not a string.");
		}
		std::string posDir = MxArray(prhs[1]).toString();
		std::string negDir = MxArray(prhs[2]).toString();
		std::string modelName = MxArray(prhs[3]).toString();
		std::string inputType = ".png";
		// Initialize detector
		MonkeyDetector monkeyDetector;
		// Train detector model
		mexPrintf("Training detector model ...\n");
		mexEvalString("drawnow;"); // to dump string.
		monkeyDetector.train(posDir, negDir, modelName, inputType);
		mexPrintf("Training complete!\n");
		mexEvalString("drawnow;"); // to dump string.
	}
	else if (par1=="detect")
	{
		if (nrhs != 3)
		{
			mexErrMsgTxt("Three input arguments required.");
		}
		if (!mxIsChar(prhs[1]))
		{
			mexErrMsgTxt("The second parameter must be a string.");
		}
		if (!mxIsNumeric(prhs[2]))
		{
			mexErrMsgTxt("The Third parameter must be numeric.");
		}
		std::vector<cv::Rect> bbox;
		std::vector<double> hitWeights;
		cv::Mat img;
		cv::Mat rgbScaled;	

		double scale = 1.0;
		//std::string modelName = "hog_model_temp.xml";
		std::string modelName = MxArray(prhs[1]).toString();
		// Read image
		img = MxArray(prhs[2]).toMat();
		// Scale image
		cv::resize(img,rgbScaled,cv::Size(0,0),scale,scale);
		// To accord with opencv convention, convert rgb to bgr image
		cv::Mat bgrScaled(rgbScaled.rows,rgbScaled.cols,CV_8UC3);
		int arr[] = {0,2, 1,1, 2,0};
		std::vector<int> fromTo(arr, arr+sizeof(arr)/sizeof(int));
		cv::mixChannels(rgbScaled, bgrScaled, fromTo);
		// Initialize detector
		MonkeyDetector monkeyDetector;
		// Load detector model
		monkeyDetector.load(modelName);
		// Detect
		monkeyDetector.detect(bgrScaled, bbox, hitWeights);
		// Get outputs
		cv::Mat outMatrix2(hitWeights);
		cv::Mat outMatrix(bbox.size(),4, CV_32FC1);	
		std::vector<cv::Rect>::const_iterator it = bbox.begin();
		int k = 0;
		while (it != bbox.end())
		{
			outMatrix.at<float>(k,0) = (float)((*it).x + 1);
			outMatrix.at<float>(k,1) = (float)((*it).y + 1);
			outMatrix.at<float>(k,2) = (float)(*it).width;
			outMatrix.at<float>(k,3) = (float)(*it).height;
			++k;
			++it;
		}

		plhs[0] = MxArray(outMatrix);
		plhs[1] = MxArray(outMatrix2);
	}
	else
	{
		mexErrMsgTxt("The first parameter must be either \"detect\" or \"train\".\n");
	}
}