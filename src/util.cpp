#include "util.h"

double getOverlapRate(cv::Rect W1,cv::Rect W2)
{
	double l = (double)max(W1.x,W2.x);
	double r = (double)min(W1.x+W1.width,W2.x+W2.width);
	double t = (double)max(W1.y,W2.y);
	double b = (double)min(W1.y+W1.height,W2.y+W2.height);
	double ovlp_area = max(r-l,0.0)*max(b-t,0.0);
	//double ovlp_ratio = max(ovlp_area/W1.area(),ovlp_area/W2.area());
	double ovlp_ratio = 2*ovlp_area/(W1.area()+W2.area());
	return ovlp_ratio;
}

int round (double x)
{
	return (int)floor(x+0.5);
}

void loadGTlog(std::string labelDir ,std::vector<std::vector<double> > &bboxGT, double Scale)
{
	double buf[4];
	int frameNo;
	std::vector<double> bbox(5,0.0);

	std::string labelFile = labelDir + "\\GTlog.txt";
	std::ifstream infile;
	infile.open(labelFile,std::ifstream::in);
	if (!infile.is_open())
		std::cout<<"File is not open. Check if it exist or not."<<std::endl;
	while(!infile.eof())
	{
		infile>>frameNo>>buf[0]>>buf[1]>>buf[2]>>buf[3];
		//std::cout<<frameNo<<" "<<buf[0]<<" "<<buf[1]<<" "<<buf[2]<<" "<<buf[3]<<std::endl;
		bbox[0] = frameNo;
		for (size_t i=0;i<4;++i)
		{			
			bbox[i+1] = Scale*buf[i];
		}
		bboxGT.push_back(bbox);
	}
	infile.close();
}

std::vector<cv::Rect> & catRect(std::vector<cv::Rect> &v1, std::vector<cv::Rect> &v2)
{
	std::vector<cv::Rect>::iterator it = v2.begin();
	while (it != v2.end())
	{
		v1.push_back(*it);
		++it;
	}
	return v1;
}

std::vector<double> & catVect(std::vector<double> & v1, std::vector<double> &v2)
{
	std::vector<double>::iterator it = v2.begin();
	while (it != v2.end())
	{
		v1.push_back(*it);
		++it;
	}
	return v1;
}

bool fexists(const std::string& name)
{
    if (FILE *file = fopen(name.c_str(), "r"))
	{
        fclose(file);
        return true;
    } else {
        return false;
    }   
}