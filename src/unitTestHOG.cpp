#include "unitTestHOG.h"

//#define LOADGT

void test_hogTraining()
{
	MonkeyDetector monkeyDetector;
	std::string posInputDir = "C:\\zxk\\Research\\monkeyData\\Camera1_labeledVideos_Patch_Pos";
	std::string negInputDir = "C:\\zxk\\Research\\monkeyData\\Camera1_labeledVideos_Patch_Neg";	
	std::string modelNameTemp = "hog_model_temp.xml";
	std::string inputType = ".png";	
	//generateHogFile(posInputDir, negInputDir,  inputType, logName, scale);
	monkeyDetector.train(posInputDir, negInputDir, modelNameTemp, inputType);
}

void test_hogDetection()
{
	std::string inputType = ".png";
	double scale=0.5;
	std::string inputDir = "C:\\zxk\\Research\\monkeyData\\Camera1_extractedVideos\\_extracted_100";
	std::string modelName = "hog_model_temp.xml";
	std::vector<std::string> inputFileNames;
	inputFileNames = getFileNameList(inputDir,inputType.c_str());

#ifdef LOADGT
	std::vector< std::vector<double> > bboxGT;
	loadGTlog(inputDir,bboxGT,scale);
#endif

	std::string outputFileName = "detection.txt";
	std::ofstream outfile;
	outfile.open(outputFileName,std::ifstream::out);
	if (!outfile.is_open())
		std::cout<<"File is not open. Check if it exist or not."<<std::endl;

	std::string fpDir = "C:\\zxk\\Research\\monkey\\monkey_cppPrj\\testHOG\\FPpatches\\";
	std::string tpDir = "C:\\zxk\\Research\\monkey\\monkey_cppPrj\\testHOG\\TPpatches\\";
	std::string outputFP,outputTP;
	char fpName[30];
	char tpName[30];
	int fpCount = 0;
	int tpCount = 0;
	bool enableSaveFP = false;
	bool enableSaveTP = false;

	cv::Mat img;
	std::vector<cv::Rect> bbox;
	std::vector<cv::Rect> bbox_pre;
	std::vector<bool> valid;
	std::vector<double> hitWeights;
	int bbox_pre_counter = 0;	

#ifdef LOADGT
	std::vector<cv::Rect> currbboxGT;
#endif

	double ov_thres = 0.1;
	//double svm_thres = 2.2; // for hog_model_fast
	double svm_thres = 1.0;	
	double grad_thres = 8.5; // used with hog_sideView_cam4_model2
	//double grad_thres = 8.0; // used with hog_sideView_cam3_model2
	double mean_thres = 80; // used with hog_sideView_cam4_model2
	//double mean_thres = 50; // used with hog_sideView_cam3_model2

	MonkeyDetector monkeyDetector;
	monkeyDetector.load(modelName);

	int frameNo = 0;

	std::vector<std::string>::const_iterator itImg = inputFileNames.begin();
	while (itImg!=inputFileNames.end())
	{
		std::cout << "Processing Frame " << frameNo << "..." << std::endl;
		img = cv::imread(*itImg);
		cv::resize(img,img,cv::Size(0,0),scale,scale);

		//hogDetect(img, w, scale, bbox, hitWeights);
		monkeyDetector.detect(img, bbox, hitWeights);

#ifdef LOADGT
		valid.assign(bbox.size(),false);
#else
		valid.assign(bbox.size(),true);
#endif


#ifdef LOADGT
		for (size_t i=0;i<bboxGT.size();++i)
		{
			if ( (int)bboxGT[i][0]==frameNo+1 )
			{
				currbboxGT.push_back(cv::Rect(round(bboxGT[i][1]),
					round(bboxGT[i][2]),round(bboxGT[i][3]),round(bboxGT[i][4])) );
			}
		}

		size_t pcount = 0;
		for (size_t i=0;i<bbox.size();++i)
		{			
			for (size_t j=0;j<currbboxGT.size();++j)
			{				
				double ov = getOverlapRate(bbox[i],currbboxGT[j]);
				if (ov>ov_thres)
				{
					++pcount;
					valid[i]=true;
					break;
				}
			}
		}
		double accuracy = (double)pcount/(double)bbox.size();
		std::cout<<accuracy<<std::endl;
#endif

		if (enableSaveFP)
		{
			for (size_t i=0; i<bbox.size(); ++i)
			{
				if (valid[i]) continue;			
				sprintf_s(fpName,"FP_%04d.png",fpCount);
				outputFP = fpDir + fpName;
				cv::Mat fpImg;
				cv::resize(img(bbox[i]),fpImg,cv::Size(60,60));
				cv::imwrite(outputFP,fpImg);
				fpCount++;
			}
		}

		if (enableSaveTP)
		{
			for (size_t i=0; i<bbox.size(); ++i)
			{
				if (!valid[i]) continue;			
				sprintf_s(tpName,"TP_%04d.png",fpCount);
				outputTP = tpDir + fpName;
				cv::imwrite(outputTP,img(bbox[i]));
				tpCount++;
			}
		}

		for (size_t i=0; i<bbox.size(); ++i)
		{
			if (valid[i])
				cv::rectangle(img,bbox[i],CV_RGB(255,0,0),1);
			else
				cv::rectangle(img,bbox[i],CV_RGB(0,0,255),1);

			outfile<<frameNo<<" "<<bbox[i].x<<" "<<bbox[i].y<<
				" "<<bbox[i].width<<" "<<bbox[i].height<<" "<<hitWeights[i]<<std::endl;
		}

		//#ifdef LOADGT
		//for (size_t i=0; i<currbboxGT.size(); ++i)
		//{
		//	cv::rectangle(img,currbboxGT[i],CV_RGB(0,255,0),1);
		//}
		//#endif

		cv::namedWindow("my detection",CV_WINDOW_AUTOSIZE);
		cv::imshow("my detection",img);
		//cv::imwrite("detection example.png",img);
		cv::waitKey(10);

		//bbox_pre.clear();
		//for(size_t i=0;i<bbox.size();++i)
		//{
		//	if(valid[i]) bbox_pre.push_back(bbox[i]);
		//}
		bbox.clear();
#ifdef LOADGT
		currbboxGT.clear();
#endif
		++itImg;
		++frameNo;
	}
	outfile.close();

}