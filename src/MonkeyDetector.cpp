#include "MonkeyDetector.h"

MonkeyDetector::MonkeyDetector()
{
	//double scaleX[] = {1.5, 2.5};
	//double scaleY[] = {1.5, 2.5};
	double a[] = {0.5, 1};
	double b[] = {0.5, 1};
	scaleX.assign(a,a+2);
	scaleY.assign(b,b+2);
	winW_orig = 40;
	winH_orig = 40;
	winStepX = 10;
	winStepY = 10;	
	nlevels = 10;
	svm_thres = 0.0;
	scaleStep = 1.1;
	merge_thres = 0.0;
	mergeEnable = false;
	w.clear();
	c = 0.1;
	winSize = cv::Size(60, 60);
	blockSize = cv::Size(20, 20);
	blockStride = cv::Size(10, 10);
	cellSize = cv::Size(10, 10);
}

MonkeyDetector::~MonkeyDetector(){}

void MonkeyDetector::detect(cv::Mat img, std::vector<cv::Rect> &bbox, std::vector<double> &hitWeights)
{
	size_t nScaleX = scaleX.size();
	size_t nScaleY = scaleY.size();
	int winW,winH;
	std::vector<cv::Rect> tempBB;
	std::vector<double> tempHitWeights;

	for (size_t indX=0; indX<nScaleX; ++indX)
	{
		for (size_t indY=0; indY<nScaleY; ++indY)
		{
			winW = int(winW_orig*scaleX[indX])/6*6;
			winH = int(winH_orig*scaleY[indY])/6*6;
			winSize = cv::Size(winW, winH);
			blockSize = cv::Size(winW/3, winH/3);
			blockStride = cv::Size(winW/6, winH/6);
			cellSize = cv::Size(winW/6, winH/6);
			detectMultiScale(img,tempBB,tempHitWeights,svm_thres,cv::Size(),cv::Size(),scaleStep,merge_thres,mergeEnable);
			bbox = catRect(bbox, tempBB);
			hitWeights = catVect(hitWeights, tempHitWeights);
		}
	}

	// filter the bounding boxes that violate color constraints		
	colorFilter(img, bbox, hitWeights);
	//// filter the bounding boxes that violate gradient constraints
	//gradientFilter(img, bbox, grad_thres);
	//// filter the bounding boxes that violate intensity mean constraints
	//meanFilter(img, bbox, mean_thres);
	// filter the bounding boxes that violate geometric constraints
	//geometricFilter(bbox,bbox_pre,valid);
}

void MonkeyDetector::train(std::string posInputDir, std::string negInputDir, std::string modelName,
	std::string inputType, std::string logName, std::string svmModelName)
{
	std::vector<std::string> inputFileNames1,inputFileNames2;
	inputFileNames1 = getFileNameList(posInputDir,inputType.c_str());
	inputFileNames2 = getFileNameList(negInputDir,inputType.c_str());

	cv::Mat img;
	std::vector<float> ders;

	if (fexists(modelName))
	{
		loadParameters(modelName);
	}

	// save hog features to a file
	std::ofstream myfile;
	myfile.open(logName.c_str());

	int i;
	std::vector<float>::const_iterator it;
	std::vector<std::string>::const_iterator itImg = inputFileNames1.begin();
	while (itImg!=inputFileNames1.end())
	{
		std::cout << "Processing Frame " << itImg-inputFileNames1.begin() << "..." << std::endl;
		img = cv::imread(*itImg);

		compute(img, ders);

		myfile << '+' << 1;		
		for(i=0,it=ders.begin(); it!=ders.end(); ++i,++it)
		{
			myfile << ' ' << i << ':' << *it;
		}
		myfile << std::endl;

		++itImg;
	}

	itImg = inputFileNames2.begin();
	while (itImg!=inputFileNames2.end())
	{
		std::cout << "Processing Frame " << itImg-inputFileNames2.begin() << "..." << std::endl;
		img = cv::imread(*itImg);

		compute(img, ders);

		myfile << '-' << 1;
		for(i=0,it=ders.begin(); it!=ders.end(); ++i,++it)
		{
			myfile << ' ' << i << ':' << *it;
		}
		myfile << std::endl;

		++itImg;
	}

	myfile.close();

	std::ostringstream convert;
	convert << c;

	std::string cmd = "svm-train -t 0 -s 0 -c ";
	cmd += convert.str();
	cmd += " ";
	cmd += logName;
	cmd += " ";
	cmd += svmModelName;

	std::cout << "training SVM model... " << std::endl;
	system(cmd.c_str());
	std::cout << "training complete! " << std::endl;

	loadSVMModel(svmModelName);
	save(modelName);
}

void MonkeyDetector::load(std::string modelName)
{
	std::vector<int> siz;
	cv::FileStorage f;
	f.open(modelName.c_str(), cv::FileStorage::READ);
	f["scaleX"] >> scaleX;
	f["scaleY"] >> scaleY;
	f["winW_orig"] >> winW_orig;
	f["winH_orig"] >> winH_orig;
	f["winStepX"] >> winStepX;
	f["winStepY"] >> winStepY;
	f["nlevels"] >> nlevels;
	f["svm_thres"] >> svm_thres;
	f["scaleStep"] >> scaleStep;
	f["merge_thres"] >> merge_thres;
	f["mergeEnable"] >> mergeEnable;
	f["w"] >> w;
	f["c"] >> c;
	f["winSize"] >> siz;
	winSize = cv::Size(siz[0],siz[1]);
	f["blockSize"] >> siz;
	blockSize = cv::Size(siz[0],siz[1]);
	f["blockStride"] >> siz;
	blockStride = cv::Size(siz[0],siz[1]);
	f["cellSize"] >> siz;
	cellSize = cv::Size(siz[0],siz[1]);
	f.release();
	setSVMDetector(w);
}

void MonkeyDetector::save(std::string modelName)
{
	cv::FileStorage f;
	f.open(modelName.c_str(), cv::FileStorage::WRITE);
	f << "scaleX" << scaleX;
	f << "scaleY" << scaleY;
	f << "winW_orig" << winW_orig;
	f << "winH_orig" << winH_orig;
	f << "winStepX" << winStepX;
	f << "winStepY" << winStepY;
	f << "nlevels" << nlevels;
	f << "svm_thres" << svm_thres;
	f << "scaleStep" << scaleStep;
	f << "merge_thres" << merge_thres;
	f << "mergeEnable" << mergeEnable;
	f << "w" << w;
	f << "c" << c;
	f << "winSize" << winSize;
	f << "blockSize" << blockSize;
	f << "blockStride" << blockStride;
	f << "cellSize" << cellSize;
	f.release();	
}

void MonkeyDetector::loadParameters(std::string modelName)
{
	std::vector<int> siz;
	cv::FileStorage f;
	f.open(modelName.c_str(), cv::FileStorage::READ);
	f["scaleX"] >> scaleX;
	f["scaleY"] >> scaleY;
	f["winW_orig"] >> winW_orig;
	f["winH_orig"] >> winH_orig;
	f["winStepX"] >> winStepX;
	f["winStepY"] >> winStepY;
	f["nlevels"] >> nlevels;
	f["svm_thres"] >> svm_thres;
	f["scaleStep"] >> scaleStep;
	f["merge_thres"] >> merge_thres;
	f["mergeEnable"] >> mergeEnable;
	f["c"] >> c;
	f["winSize"] >> siz;
	winSize = cv::Size(siz[0],siz[1]);
	f["blockSize"] >> siz;
	blockSize = cv::Size(siz[0],siz[1]);
	f["blockStride"] >> siz;
	blockStride = cv::Size(siz[0],siz[1]);
	f["cellSize"] >> siz;
	cellSize = cv::Size(siz[0],siz[1]);
	f.release();	
}

void MonkeyDetector::saveParameters(std::string modelName)
{
	cv::FileStorage f;
	f.open(modelName.c_str(), cv::FileStorage::WRITE);
	f << "scaleX" << scaleX;
	f << "scaleY" << scaleY;
	f << "winW_orig" << winW_orig;
	f << "winH_orig" << winH_orig;
	f << "winStepX" << winStepX;
	f << "winStepY" << winStepY;
	f << "nlevels" << nlevels;
	f << "svm_thres" << svm_thres;
	f << "scaleStep" << scaleStep;
	f << "merge_thres" << merge_thres;
	f << "mergeEnable" << mergeEnable;
	f << "c" << c;
	f << "winSize" << winSize;
	f << "blockSize" << blockSize;
	f << "blockStride" << blockStride;
	f << "cellSize" << cellSize;
	f.release();	
}

void MonkeyDetector::loadSVMModel(std::string modelName)
{
	struct svm_model * pModel = new struct svm_model;
	pModel = svm_load_model(modelName.c_str());

	const double * const *sv_coef = pModel->sv_coef;
	const svm_node * const *SV = pModel->SV;
	int l = pModel->l;
	pModel->label;

	const svm_node* p_tmp = SV[0];
	int len = 0;
	while( p_tmp->index != -1 )
	{
		len++;
		p_tmp++;
	}
	w.clear();
	w.resize( len+1 );

	for( int i=0; i<l; i++)
	{
		double svcoef = sv_coef[0][i];
		const svm_node* p = SV[i];
		while( p->index != -1 )
		{
			w[p->index] += float(svcoef * p->value);
			p++;
		}
	}
	w[len] = float(-pModel->rho[0]);
	setSVMDetector(w);
	delete pModel;
}

void MonkeyDetector::colorFilter(cv::Mat &img, std::vector<cv::Rect> &bbox)
{
	cv::Mat croppedImg;
	cv::Mat hsv;
	int nTotalPixels;	
	int redPixelCount;
	int bluePixelCount;
	double hue;
	double redTH = 0.06;
	double redPercent = 0.055;
	double blueTH = 0.5;
	double bluePercent = 0.06;
	for (size_t k=0; k<bbox.size(); ++k)
	{
		croppedImg = img(bbox[k]);
		cv::cvtColor(croppedImg, hsv, CV_BGR2HSV);
		nTotalPixels = hsv.rows*hsv.cols;
		redPixelCount = 0;
		bluePixelCount = 0;
		for(int i=0;i<hsv.rows;++i)
		{
			for(int j=0;j<hsv.cols;++j)
			{
				hue = (double)hsv.at<cv::Vec3b>(i,j)[0]/180;				
				if (hue < redTH) ++redPixelCount;
				if (hue > blueTH) ++bluePixelCount;
			}
		}
		if ( (double)redPixelCount/(double)nTotalPixels > redPercent  ||
			(double)bluePixelCount/(double)nTotalPixels > bluePercent )
		{
			bbox.erase(bbox.begin()+k);			
			--k;
		}
	}	
}

void MonkeyDetector::colorFilter(cv::Mat &img, std::vector<cv::Rect> &bbox, std::vector<double> &hitWeights)
{
	cv::Mat croppedImg;
	cv::Mat hsv;
	int nTotalPixels;	
	int redPixelCount;
	int bluePixelCount;
	double hue;
	double redTH = 0.06;
	double redPercent = 0.055;
	double blueTH = 0.5;
	double bluePercent = 0.06;
	for (size_t k=0; k<bbox.size(); ++k)
	{
		croppedImg = img(bbox[k]);
		cv::cvtColor(croppedImg, hsv, CV_BGR2HSV);
		nTotalPixels = hsv.rows*hsv.cols;
		redPixelCount = 0;
		bluePixelCount = 0;
		for(int i=0;i<hsv.rows;++i)
		{
			for(int j=0;j<hsv.cols;++j)
			{
				hue = (double)hsv.at<cv::Vec3b>(i,j)[0]/180;				
				if (hue < redTH) ++redPixelCount;
				if (hue > blueTH) ++bluePixelCount;
			}
		}
		if ( (double)redPixelCount/(double)nTotalPixels > redPercent  ||
			(double)bluePixelCount/(double)nTotalPixels > bluePercent )
		{
			bbox.erase(bbox.begin()+k);
			hitWeights.erase(hitWeights.begin()+k);
			--k;
		}
	}	
}