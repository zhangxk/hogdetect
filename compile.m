clc; clear all; 

if ispc
    disp('PC');
    include = ' -IC:\opencv\build\include\opencv\ -IC:\opencv\build\include\ -I.\include\';
    C = computer;    
    if strcmp(C,'PCWIN')
        disp('32 bit windows machine');
        libPath = 'C:\opencv\build\x86\vc10\lib\';
        files = dir([libPath '*.lib']);
    elseif strcmp(C,'PCWIN64')
        disp('64 bit windows machine');
        libPath = 'C:\opencv\build\x64\vc10\lib\';
        files = dir([libPath '*.lib']);
    end
    
    lib = [];
    for i = 1:length(files),
        lib = [lib ' ' libPath files(i).name];
    end
    
    srcPath = '.\src\';
    files = dir([srcPath '*.cpp']);
    src = [];
    for i = 1:length(files),
        src = [src ' ' srcPath files(i).name];
    end
    
    eval(['mex .\src\main.cpp -O -v -output hogDetect' include lib src]);
    
    disp('Compilation finished.');
end