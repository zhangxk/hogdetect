% hogDetect unit test
function test

% test train

addpath(genpath('.\xml_io_tools'));
modelName = 'hog_test_model.xml';
posInputDir = '.\posExamples';
negInputDir = '.\negExamples';

parameters.scaleX = [0.5000 1];
parameters.scaleY = [0.5000 1];
parameters.winW_orig = 80;
parameters.winH_orig = 80;
parameters.winStepX = 10;
parameters.winStepY = 10;
parameters.nlevels = 5;
parameters.svm_thres = 0.0;
parameters.scaleStep = 1.1000;
parameters.merge_thres = 0.0;
parameters.mergeEnable = 0;
parameters.c = 0.01;
parameters.winSize = [60 60];
parameters.blockSize = [20 20];
parameters.blockStride = [10 10];
parameters.cellSize = [10 10];

xml_write(modelName, parameters, 'opencv_storage');
hogDetect('train', posInputDir, negInputDir, modelName);

% test detect
I = imread('cam1_sample.png');
I = imresize(I,0.5);
[bbox,weights] = hogDetect('detect','hog_test_model.xml',I);
if ~isempty(bbox)
    nonMaxSupWinSiz = 30;
    weightTH = 0.2;
    [bbox,weights] = groupBBox_nonMaxSup(bbox,weights,nonMaxSupWinSiz);
    [bbox,weights] = thresholdFilter(bbox,weights,weightTH);
end
figure(1);
imshow(I);
for i=1:size(bbox,1)
    hold on; rectangle('Position',bbox(i,:),'EdgeColor','r'); hold off;
end

end